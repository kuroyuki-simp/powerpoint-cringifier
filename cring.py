from pptx import Presentation
from pptx.dml.color import RGBColor
from pptx.util import Pt
from pptx.shapes.picture import Picture
from itertools import cycle
from io import BytesIO
from PIL import Image, ImageSequence
from random import randint
pres = Presentation("test.pptx")



col = cycle([
    RGBColor(255,0,0),
    RGBColor(255,215,0),
    RGBColor(255,255,0),
    RGBColor(0,255,0),
    RGBColor(0,0,255),
    RGBColor(128,0,128)
])
it = False
bl = True
for slide in pres.slides:
    if slide.shapes.title:
        text_frame = slide.shapes.title.text_frame
        text = text_frame.text
        text_frame.clear()
        p = text_frame.paragraphs[0]
        for char in text:
            run = p.add_run()
            run.text = char

            font = run.font
            font.name = 'Comic Sans MS'
            bl = not bl
            font.bold = bl
            it = not it
            font.italic = it  # cause value to be inherited from theme
            font.color.rgb = next(col)
        
    for i in slide.shapes:
        if isinstance(i, Picture):
            bio = BytesIO()
            if randint(1,2) == 1:
                smiley = Image.open("smiley.png")
                im = Image.open(BytesIO(i.image.blob))
                bg_w, bg_h = im.size
                basewidth = int(bg_w / 3)
                wpercent = (basewidth/float(smiley.size[0]))
                hsize = int((float(smiley.size[1])*float(wpercent)))
                smiley = smiley.resize((basewidth,hsize), Image.ANTIALIAS)
                overlay_w, overlay_h = smiley.size
                offset = ((bg_w - overlay_w), (bg_h - overlay_h))
                im.paste(smiley, offset, smiley)
                im.save(bio, 'GIF')
            else:
                kgif = Image.open("deltarune-kris.gif")
                frames = []
                im = Image.open(BytesIO(i.image.blob))
                bg_w, bg_h = im.size
                basewidth = int(bg_w / 3)
                wpercent = (basewidth/float(kgif.size[0]))
                hsize = int((float(kgif.size[1])*float(wpercent)))
                kgif.seek(1)
                kgif.save("Dez.gif", save_all=True, append_frames=list(ImageSequence.Iterator(kgif)))
                for kris in ImageSequence.Iterator(kgif):
                    kris = kris.convert("RGBA")
                    kris = kris.resize((basewidth,hsize))
                    nim = im.copy().convert("RGBA")
                    overlay_w, overlay_h = kris.size
                    offset = ((bg_w - overlay_w) // 2, (bg_h - overlay_h) // 2)
                    nim.paste(kris, offset, kris)
                    nim.save("Frame.gif")
                    frames.append(nim)
                im = frames[0]
                im.save(bio, 'GIF', save_all=True, append_images=frames, loop=0)
                im.save("testGif.gif", 'GIF', save_all=True, append_images=frames, loop=0)

            def _replace_picture(slide_obj, shape_obj, r_img_blob):
                # noinspection PyProtectedMember
                img_pic = shape_obj._pic
                img_rid = img_pic.xpath("./p:blipFill/a:blip/@r:embed")[0]
                img_part = slide_obj.part.related_part(img_rid)
                img_part._blob = r_img_blob
            _replace_picture(slide, i, bio.getvalue())
        if not i.has_text_frame:
            continue
        for para in i.text_frame.paragraphs:
            para.font.name = 'Comic Sans MS'

pres.save("cring.pptx")